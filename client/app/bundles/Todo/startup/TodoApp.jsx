import React from 'react'
import ReactOnRails from 'react-on-rails'
import { Provider } from 'react-redux'
import App from '../components/App'
import configureStore from '../store/todoStore';

const TodoApp = (props, _railsContext) => (
  <Provider store={configureStore(props)}>
    <App />
  </Provider>
)

ReactOnRails.register({ TodoApp })
