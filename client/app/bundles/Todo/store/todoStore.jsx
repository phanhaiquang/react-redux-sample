import { createStore } from 'redux';
import todoReducer from '../reducers'

const configureTodoStore = (railsProps) => (
  createStore(todoReducer, railsProps)
);

export default configureTodoStore;
